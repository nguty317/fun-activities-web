﻿<%@ Page Title="Activity List 1" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ActivityList1.aspx.cs" Inherits="Assignment_1_nguty317.ActivityList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPBody" runat="server">
    <br />
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9">
                <h1 class="text-center">Fun things to do when you're quarantined</h1>
                <p>
                    We're all in this together. Whether you're self-isolating, social distancing or just plain WFH, here's some tips to keep you sane and occupied during the pandemic.</p>
            </div>
        </div>


        <div class="row justify-content-end">
            <div class="col-5">
                <div class="form-inline my-2 my-lg-0">
                    <input class="form-control" type="text" name="searchText" placeholder="Search..." aria-label="Search">
                    <asp:Button Style="margin-right: 10px;" class="btn btn-outline-success my-2 my-sm-0" ID="BSearch" runat="server" Text="Search" OnClick="Search" ToolTip="You can search by keywords, category or time of the day!"/>
                    <asp:SqlDataSource ID="SDSSearch" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT A.[activityId], A.[name], A.[description], A.[imageUrl], C.[category], T.[time] as recommendedTime
FROM [Activity] AS A
JOIN [ActivityCategory] AS C ON A.categoryId = C.categoryId
JOIN [Time] AS T ON A.recommendedTimeId = T.timeId
WHERE (A.[name] LIKE '%' + @search + '%')
OR (A.[description] LIKE '%' + @search + '%')
OR (C.[category] LIKE '%' + @search + '%')
OR (T.[time] LIKE '%' + @search + '%')">
                        <SelectParameters>
                            <asp:FormParameter FormField="searchText" Name="search" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Button class="btn btn-outline-warning my-2 my-sm-0" ID="BLucky" runat="server" Text="I'm feeling lucky" OnClick="Lucky" ToolTip="It generates a random activity for you!" />
                    <asp:SqlDataSource ID="SDSLucky" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT A.[activityId], A.[name], A.[description], A.[imageUrl], C.[category], T.[time] as recommendedTime
FROM [Activity] AS A
JOIN [ActivityCategory] AS C ON A.categoryId = C.categoryId
JOIN [Time] AS T ON A.recommendedTimeId = T.timeId
WHERE A.[activityId] LIKE (SELECT FLOOR(RAND()*14)+1)"></asp:SqlDataSource>
                </div>
            </div>
        </div>


        <p id="noResult" runat="server" class="text-center"><i>We can't find any results :(</i></p>
        <div class="card-columns">
            <asp:Repeater ID="RActivityList" runat="server">
                <ItemTemplate>
                    <div class="card" style="width: 20rem;">
                        <a target="_blank" href="ViewActivity.aspx?id=<%# Eval("activityId") %>" class="stretched-link"></a>
                        <img class="card-img-top align-self-center" src='<%# Eval("imageUrl") %>'
                            alt='<%# Eval("name") %>' />
                        <h5 class="card-header text-white text-center">
                            <%# Eval("name") %>
                        </h5>
                        <div class="card-body">
                            <p class="card-text">
                                <%# Convert.ToString(Eval("description")).Length > 100 
                                            ? Convert.ToString(Eval("description")).Substring(0, 100) + "..." 
                                            : Eval("description") %>
                                <hr />
                                <strong>Category: </strong><%# Eval("category") %>
                                <br />
                                <strong>Recommended Time: </strong><%# Eval("recommendedTime") %>
                            </p>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>


            <asp:SqlDataSource ID="SDSActivityList" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT A.[activityId], A.[name], A.[description], A.[imageUrl], C.[category], T.[time] as recommendedTime
FROM [Activity] AS A
JOIN [ActivityCategory] AS C ON A.categoryId = C.categoryId
JOIN [Time] AS T ON A.recommendedTimeId = T.timeId
"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
