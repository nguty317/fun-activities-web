﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="Assignment_1_nguty317.HomePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPBody" runat="server">
    <br />
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9 text-center">
                <hr />
                <h1>Welcome to Hazel's fun activities website</h1>
                <p>Discover fun things to do today!</p>
                
                <div class="btn-group">
                    <a class="btn btn-info" role="button" href="ActivityList1.aspx">Activity List 1</a>
                    <a class="btn btn-info" role="button" href="ActivityList2.aspx">Activity List 2</a>
                    <a class="btn btn-info" role="button" href="/Activities/Index">Activity List 3</a>
                </div>
                <hr />
            </div>
        </div>
        <br />
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active" data-interval="3000">
                    <img src="/image/14.jpg" class="d-block w-100" alt="Activity carousel!">
                </div>
                <div class="carousel-item" data-interval="3000">
                    <img src="/image/9.jpg" class="d-block w-100" alt="Activity carousel!">
                </div>
                <div class="carousel-item" data-interval="3000">
                    <img src="/image/13.png" class="d-block w-100" alt="Activity carousel!">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
