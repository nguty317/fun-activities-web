﻿<%@ Page  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewActivity.aspx.cs" Inherits="Assignment_1_nguty317.ViewActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPBody" runat="server">
    <div class="container">
        <div class="row justify-content-center">

                <fieldset>
                    <asp:FormView runat="server" ID="FVActivity"
                        DataKeyNames="activityId"
                        RenderOuterTable="false"
                        ItemType="Assignment_1_nguty317.Models.Activity"
                        SelectMethod="FVActivity_GetItem">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-6">
                                    <h2 class="text-center" style="margin-top: 20px">Activity # <%# Item.activityId.ToString() %>: <%# Item.name %></h2>
                                    <br />
                                    <div class="form-group" id="viewActivity">
                                        <label>Difficulty Level: </label>
                                        <%# Item.difficultyLevel %>
                                    </div>
                                    <div class="form-group">
                                        <label>Description: </label>
                                        <p><%# Item.description %></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Category: </label>
                                        <%# Item.ActivityCategory.category %>
                                    </div>
                                    <div class="form-group">
                                        <label>Recommended time: </label>
                                        <%# Item.Time.time %>
                                    </div>
                                    <label>LOL</label>

                                    <div class="form-group">
                                        <a href="#" onclick="location.href = document.referrer; return false;" class="btn btn-info">Back To Activities</a>
                                    </div>
                                </div>
                                <div class="imageContainer col-6">
                                    <div class="imageHolder">
                                        <img class="viewActivityImage" src="<%# Item.imageUrl %>" alt="<%# Item.name %>" />
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <legend>You didn't choose any activity.
                                <br />
                                Would you like to <a href="#">return to Activity List</a>?
                            </legend>

                        </EmptyDataTemplate>
                    </asp:FormView>

                </fieldset>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
