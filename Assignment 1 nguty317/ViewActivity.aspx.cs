﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_1_nguty317
{
    public partial class ViewActivity : System.Web.UI.Page
    {
        Models.ActivityEntities db = new Models.ActivityEntities();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // The id parameter should match the DataKeyNames value set on the control
        // or be decorated with a value provider attribute, e.g. [QueryString]int id
        public Assignment_1_nguty317.Models.Activity FVActivity_GetItem([QueryString]int? id)
        {
            Models.Activity item = null;
            if (id.HasValue)
            {
                item = (from a in db.Activities
                        join c in db.ActivityCategories on a.categoryId equals c.categoryId
                        join t in db.Times on a.recommendedTimeId equals t.timeId
                        where a.activityId == id.Value
                        select a).FirstOrDefault();
            }
            Page.Title = "Activity " + item.activityId.ToString() + ": " + item.name; 
            return item;
        }
       
    }

}