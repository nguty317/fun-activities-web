﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_1_nguty317
{
    public partial class ActivityList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RActivityList.DataSource = SDSActivityList;
            RActivityList.DataBind();
            noResult.Visible = false;
        }

        protected void Search(object sender, EventArgs e)
        {
            RActivityList.DataSource = SDSSearch;
            RActivityList.DataBind();
            DataView d = (DataView)SDSSearch.Select(DataSourceSelectArguments.Empty);
            if (d == null || d.Count == 0)
            {
                noResult.Visible = true;
            }
        }

        protected void Lucky(object sender, EventArgs e)
        {
            RActivityList.DataSource = SDSLucky;
            RActivityList.DataBind();
        }
    }
}