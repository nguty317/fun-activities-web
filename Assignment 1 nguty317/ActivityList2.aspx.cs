﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Assignment_1_nguty317.Models;

namespace Assignment_1_nguty317
{
    public partial class ActivityList2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            noResult.Visible = false;
        }

        protected void Search(object sender, EventArgs e)
        {
            RActivityList.SelectMethod = "RActivityList_Search";
            IEnumerable<Assignment_1_nguty317.Models.Activity> searchResult = RActivityList_Search();
            if (searchResult.Count() == 0)
            {
                noResult.Visible = true;
            }

        }
        
        protected void Lucky(object sender, EventArgs e)
        {
            RActivityList.SelectMethod = "RActivityList_Lucky";
        }

    

        public IEnumerable<Assignment_1_nguty317.Models.Activity> RActivityList_GetData()
        {
            var db = new ActivityEntities();

            var activity = (from a in db.Activities
                            join c in db.ActivityCategories on a.categoryId equals c.categoryId
                            join t in db.Times on a.recommendedTimeId equals t.timeId
                            select a);
            return activity;
        }

        
        public IEnumerable<Assignment_1_nguty317.Models.Activity> RActivityList_Search()
        {
            var db = new ActivityEntities();
            string searchText = TBSearch.Text;

            if (!String.IsNullOrEmpty(searchText))
            {
                var activity = (from a in db.Activities
                                join c in db.ActivityCategories on a.categoryId equals c.categoryId
                                join t in db.Times on a.recommendedTimeId equals t.timeId
                                where a.name.Contains(searchText) || a.description.Contains(searchText) || 
                                      a.ActivityCategory.category.Contains(searchText) || a.Time.time.Contains(searchText)
                                select a);
                return activity;
            } else
            {
                return RActivityList_GetData();
            }
        }
        

        public IEnumerable<Assignment_1_nguty317.Models.Activity> RActivityList_Lucky()
        {
            var db = new ActivityEntities();
            Random rand = new Random();
            int randInt = rand.Next(1, 15);

            var activity = (from a in db.Activities
                            join c in db.ActivityCategories on a.categoryId equals c.categoryId
                            join t in db.Times on a.recommendedTimeId equals t.timeId
                            where a.activityId == randInt
                            select a);
            return activity;
        }
    }
}