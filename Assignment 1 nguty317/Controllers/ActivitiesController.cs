﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_1_nguty317.Models;

namespace Assignment_1_nguty317.Controllers
{
    public class ActivitiesController : Controller
    {
        private ActivityEntities db = new ActivityEntities();

        // GET: Activities
        public ActionResult Index(string searchText)
        {
            ViewBag.Title = "Activity List 3";
            ViewBag.Message = "We're all in this together. Whether you're self-isolating, social distancing or just plain WFH, here's some tips to keep you sane and occupied during the pandemic.";

            if (!String.IsNullOrEmpty(searchText))
            {
                var activity = db.Activities.Where(a => a.name.Contains(searchText) || a.description.Contains(searchText) ||
                                      a.ActivityCategory.category.Contains(searchText) || a.Time.time.Contains(searchText));
                return View(activity.ToList());
            }
            return View(db.Activities.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
