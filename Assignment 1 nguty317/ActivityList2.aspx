﻿<%@ Page Title="Activity List 2" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ActivityList2.aspx.cs" Inherits="Assignment_1_nguty317.ActivityList2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPBody" runat="server">
    <br />
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9">
                <h1 class="text-center">Fun things to do when you're quarantined</h1>
                <p>
                    We're all in this together. Whether you're self-isolating, social distancing or just plain WFH, here's some tips to keep you sane and occupied during the pandemic.
                </p>
            </div>
        </div>


        <div class="row justify-content-end">
            <div class="col-5">
                <div class="form-inline my-2 my-lg-0">
                    <asp:TextBox ID="TBSearch" runat="server" class="form-control" type="text" name="searchText" placeholder="Search..." aria-label="Search"></asp:TextBox>
                    <asp:Button Style="margin-right: 10px;" class="btn btn-outline-success my-2 my-sm-0" ID="BSearch" runat="server" Text="Search" OnClick="Search" />
                    <asp:Button class="btn btn-outline-warning my-2 my-sm-0" ID="BLucky" runat="server" Text="I'm feeling lucky" OnClick="Lucky" ToolTip="It generates a random activity for you!" />
                </div>
            </div>
        </div>


        <p id="noResult" runat="server" class="text-center"><i>We can't find any results :(</i></p>
        <div class="card-columns">

            <asp:Repeater ID="RActivityList" runat="server" ItemType="Assignment_1_nguty317.Models.Activity" SelectMethod="RActivityList_GetData">
                <ItemTemplate>
                    <div class="card" style="width: 20rem;">
                        <a target="_blank" href="ViewActivity.aspx?id=<%# Item.activityId %>" class="stretched-link"></a>
                        <img class="card-img-top" src='<%# Item.imageUrl %>'
                            alt='<%# Item.name %>' />
                        <h5 class="card-header text-white text-center">
                            <%# Item.name %>
                        </h5>

                        <div class="card-body">
                            <p class="card-text">
                                <%# Convert.ToString(Item.description).Length > 100 
                                        ? Convert.ToString(Item.description).Substring(0, 100) + "..." 
                                        : Item.description
                                %>
                                <hr />
                                <strong>Category: </strong><%# Item.ActivityCategory.category %>
                                <br />
                                <strong>Recommended Time: </strong><%# Item.Time.time %>
                            </p>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
